# Sample mapp Database Project

첫 작성일   : 2022/9/8
마지막 수정일: 2022/9/8

MySQL + mapp Database 연동을 확인하기 위한 테스트용 샘플 프로젝트입니다.

Tool 
- B&R Automation Studio 4.8 이상
- MySQL

프로젝트 1
- 일반 테스트
- 프로젝트 위치: Project/ myDB_AS48_ms5.14
- "Sim" Configuration 사용
- Automation Studio 4.8
- mapp Service 5.14

프로젝트 2
- mappView의 database widget 사용
- 화면에서 한글 입력 및 출력 가능
- 프로젝트 위치: Project/ myDB_AS411_ms5.15mv
- "Sim" Configuration 사용
- Automation Studio 4.11
- mapp Service 5.15
- mapp View 5.15, IP: 127.0.0.1:81
 
 자세한 내용은 추후 업데이트 예정...

