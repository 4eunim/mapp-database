
PROGRAM _INIT

	MpDatabaseCore_0.MpLink		:= ADR(gDatabaseCore);
	MpDatabaseQuery_0.MpLink	:= ADR(gDatabaseCore);
	MpDatabaseCore_0.Enable		:= TRUE;
	MpDatabaseQuery_0.Enable 	:= TRUE;
	//QueryName : SetData, GetData, DeleteColumn
	MpDatabaseQuery_0.Name		:= ADR(QueryName);
	
	//wstrInputData:= "??";
	//??? korean letter for worker in bank
	mem_sor[0] := 51008; 
	mem_sor[1] := 54665;
	mem_sor[2] := 50896;
	
END_PROGRAM

PROGRAM _CYCLIC

	GetData;
	JobData;
	UpdateData;
	deleteID;
	updateID;
	
	wstrData := GetData[0].address;
	memcpy(ADR(mem_dest), ADR(wstrData),SIZEOF(wstrData));

	
	
	
	//input
	IF bSet THEN
		memcpy(ADR(wstrInputData), ADR(mem_sor),SIZEOF(wstrInputData));	
		bSet := FALSE;
	END_IF;
	UpdateData.address := wstrInputData;
	
	
	
	MpDatabaseCore_0();
	MpDatabaseQuery_0();
	
END_PROGRAM

PROGRAM _EXIT
	
	MpDatabaseCore_0.Enable	:= FALSE;
	MpDatabaseCore_0();
	MpDatabaseQuery_0.Enable	:= FALSE;
	MpDatabaseQuery_0(); 
END_PROGRAM

