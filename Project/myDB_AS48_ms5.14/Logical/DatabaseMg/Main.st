
PROGRAM _INIT

	MpDatabaseCore_0.MpLink		:= ADR(gDatabaseCore);
	MpDatabaseQuery_0.MpLink	:= ADR(gDatabaseCore);
	MpDatabaseCore_0.Enable		:= TRUE;
	MpDatabaseQuery_0.Enable 	:= TRUE;
	//QueryName : SetData, GetData, DeleteColumn
	MpDatabaseQuery_0.Name		:= ADR(QueryName);
	
END_PROGRAM

PROGRAM _CYCLIC

	GetData;
	JobData;
	deleteID;
	
	MpDatabaseCore_0();
	MpDatabaseQuery_0();
	
END_PROGRAM

PROGRAM _EXIT
	
	MpDatabaseCore_0.Enable	:= FALSE;
	MpDatabaseCore_0();
	MpDatabaseQuery_0.Enable	:= FALSE;
	MpDatabaseQuery_0(); 
END_PROGRAM

